package de.hitec.nhplus.controller;

import de.hitec.nhplus.datastorage.DaoFactory;
import de.hitec.nhplus.datastorage.PatientDao;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.FileChooser;
import de.hitec.nhplus.model.Patient;
import de.hitec.nhplus.utils.DateConverter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;


/**
 * The AllPatientController contains the entire logic of the patient view. It determines which data is displayed and how to react to events.
 */
public class AllPatientController {

    @FXML
    private TableView<Patient> tableView;

    @FXML
    private TableColumn<Patient, Integer> columnId;

    @FXML
    private TableColumn<Patient, String> columnFirstName;

    @FXML
    private TableColumn<Patient, String> columnSurname;

    @FXML
    private TableColumn<Patient, String> columnDateOfBirth;

    @FXML
    private TableColumn<Patient, String> columnCareLevel;

    @FXML
    private TableColumn<Patient, String> columnRoomNumber;

    @FXML
    private Button buttonDelete;

    @FXML
    private Button buttonAdd;

    @FXML
    private Button buttonExport;

    @FXML
    private TextField textFieldSurname;

    @FXML
    private TextField textFieldFirstName;

    @FXML
    private DatePicker datePicker;

    @FXML
    private TextField textFieldCareLevel;

    @FXML
    private TextField textFieldRoomNumber;

    private final ObservableList<Patient> patients = FXCollections.observableArrayList();
    private PatientDao dao;

    /**
     * Default constructor for AllPatientController
     */
    public AllPatientController(){}

    /**
     * When initialize() gets called, all fields are already initialized. For example from the FXMLLoader
     * after loading an FXML-File. At this point of the lifecycle of the Controller, the fields can be accessed and
     * configured.
     */
    public void initialize() {
        this.readAllAndShowInTableView();

        this.columnId.setCellValueFactory(new PropertyValueFactory<>("pid"));

        // CellValueFactory to show property values in TableView
        this.columnFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        // CellFactory to write property values from with in the TableView
        this.columnFirstName.setCellFactory(TextFieldTableCell.forTableColumn());

        this.columnSurname.setCellValueFactory(new PropertyValueFactory<>("surname"));
        this.columnSurname.setCellFactory(TextFieldTableCell.forTableColumn());

        this.columnDateOfBirth.setCellValueFactory(new PropertyValueFactory<>("dateOfBirth"));
        this.columnDateOfBirth.setCellFactory(TextFieldTableCell.forTableColumn());

        this.columnCareLevel.setCellValueFactory(new PropertyValueFactory<>("careLevel"));
        this.columnCareLevel.setCellFactory(TextFieldTableCell.forTableColumn());

        this.columnRoomNumber.setCellValueFactory(new PropertyValueFactory<>("roomNumber"));
        this.columnRoomNumber.setCellFactory(TextFieldTableCell.forTableColumn());

        this.tableView.setItems(this.patients);

        this.buttonDelete.setDisable(true);
        this.buttonExport.setDisable(true);
        this.tableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Patient>() {
            @Override
            public void changed(ObservableValue<? extends Patient> observableValue, Patient oldPatient, Patient newPatient) {
                AllPatientController.this.buttonDelete.setDisable(newPatient == null);
                AllPatientController.this.buttonExport.setDisable(newPatient == null);
            }
        });

        this.buttonAdd.setDisable(true);
        ChangeListener<String> inputNewPatientListener = (observableValue, oldText, newText) ->
                AllPatientController.this.buttonAdd.setDisable(!AllPatientController.this.areInputDataValid());
        this.textFieldSurname.textProperty().addListener(inputNewPatientListener);
        this.textFieldFirstName.textProperty().addListener(inputNewPatientListener);
        this.datePicker.valueProperty().addListener((observableValue, localDate, t1) -> AllPatientController.this.buttonAdd.setDisable(!AllPatientController.this.areInputDataValid()));
        this.textFieldCareLevel.textProperty().addListener(inputNewPatientListener);
        this.textFieldRoomNumber.textProperty().addListener(inputNewPatientListener);
    }

    /**
     * When a cell of the column with first names was changed, this method will be called, to persist the change.
     *
     * @param event Event including the changed object and the change.
     */
    @FXML
    public void handleOnEditFirstname(TableColumn.CellEditEvent<Patient, String> event) {
        event.getRowValue().setFirstName(event.getNewValue());
        this.doUpdate(event);
    }

    /**
     * When a cell of the column with surnames was changed, this method will be called, to persist the change.
     *
     * @param event Event including the changed object and the change.
     */
    @FXML
    public void handleOnEditSurname(TableColumn.CellEditEvent<Patient, String> event) {
        event.getRowValue().setSurname(event.getNewValue());
        this.doUpdate(event);
    }

    /**
     * When a cell of the column with dates of birth was changed, this method will be called, to persist the change.
     *
     * @param event Event including the changed object and the change.
     */
    @FXML
    public void handleOnEditDateOfBirth(TableColumn.CellEditEvent<Patient, String> event) {
        event.getRowValue().setDateOfBirth(event.getNewValue());
        this.doUpdate(event);
    }

    /**
     * When a cell of the column with care levels was changed, this method will be called, to persist the change.
     *
     * @param event Event including the changed object and the change.
     */
    @FXML
    public void handleOnEditCareLevel(TableColumn.CellEditEvent<Patient, String> event) {
        event.getRowValue().setCareLevel(event.getNewValue());
        this.doUpdate(event);
    }

    /**
     * When a cell of the column with room numbers was changed, this method will be called, to persist the change.
     *
     * @param event Event including the changed object and the change.
     */
    @FXML
    public void handleOnEditRoomNumber(TableColumn.CellEditEvent<Patient, String> event) {
        event.getRowValue().setRoomNumber(event.getNewValue());
        this.doUpdate(event);
    }


    /**
     * Updates a patient by calling the method update() of {@link PatientDao}.
     *
     * @param event Event including the changed object and the change.
     */
    private void doUpdate(TableColumn.CellEditEvent<Patient, String> event) {
        try {
            this.dao.update(event.getRowValue());
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Reloads all patients to the table by clearing the list of all patients and filling it again by all persisted
     * patients, delivered by {@link PatientDao}.
     */
    private void readAllAndShowInTableView() {
        this.patients.clear();
        this.dao = DaoFactory.getDaoFactory().createPatientDAO();
        try {
            this.patients.addAll(this.dao.readAll());
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * This method handles events fired by the button to delete patients. It calls {@link PatientDao} to delete the
     * patient from the database and removes the object from the list, which is the data source of the
     * TableView.
     */
    @FXML
    public void handleDelete() {
        Patient selectedPatient = this.tableView.getSelectionModel().getSelectedItem();

        if (selectedPatient == null ) {
            return;
        }

        if (isDeletePossible(selectedPatient)) {
            try {
                DaoFactory.getDaoFactory().createPatientDAO().deleteById(selectedPatient.getPid());
                this.tableView.getItems().remove(selectedPatient);
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        } else {
            blockPatient(selectedPatient);
            readAllAndShowInTableView();
        }
    }

    /**
     * When this method is called it will open a Alert window with a message for the user if the user clicks
     * the button "Blockieren" it will read the pid for the user and calls the method blockPatient.
     *
     * @param selectedPatient holds the data of the selected patient that the user wants to Delete/block
     */
    private void blockPatient(Patient selectedPatient) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Löschen nicht möglich");
        alert.setHeaderText("Das Löschen dieser Daten ist nicht möglich");
        alert.setContentText("Möchten Sie die Daten blockieren?");
        ButtonType buttonBlock = new ButtonType("Blockieren");
        ButtonType buttonCancel = new ButtonType("Abbrechen", ButtonBar.ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonBlock, buttonCancel);
        alert.showAndWait().ifPresent(response -> {
            if (response == buttonBlock) {
                dao.blockPatient(selectedPatient.getPid());
            }
        });
    }

    /**
     * When this method is called it will take the createdOn and checks if its 10 years ago and returns a True or false.
     *
     * @param selectedPatient Holds the data of the Patient the user Selected
     * @return will return true or false depends on the patient possible to be deleted
     */
    private boolean isDeletePossible(Patient selectedPatient) {
        LocalDate selectedPatientLocalDate = DateConverter.convertStringToLocalDate(selectedPatient.getCreatedOn());
        LocalDate timePlusTenYears = selectedPatientLocalDate.plusYears(10);
        return LocalDate.now().isAfter(timePlusTenYears);
    }

    /**
     * This method handles the events fired by the button to add a patient. It collects the data from the
     * TextField>s, creates an object of class Patient of it and passes the object to
     * {@link PatientDao} to persist the data.
     */
    @FXML
    public void handleAdd() {
        String surname = this.textFieldSurname.getText();
        String firstName = this.textFieldFirstName.getText();
        LocalDate date = this.datePicker.getValue();
        String careLevel = this.textFieldCareLevel.getText();
        String roomNumber = this.textFieldRoomNumber.getText();
        LocalDate createdOn = LocalDate.now();
        try {
            this.dao.create(new Patient(firstName, surname, date, careLevel, roomNumber, createdOn, 0));
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        readAllAndShowInTableView();
        clearTextfields();
    }

    /**
     * Clears all contents from all TextFields.
     */
    private void clearTextfields() {
        this.textFieldFirstName.clear();
        this.textFieldSurname.clear();
        this.datePicker.cancelEdit();
        this.textFieldCareLevel.clear();
        this.textFieldRoomNumber.clear();
    }

    /**
     * Checks if the User gave a Valide date and if all Textfields have a Value in them.
     *
     * @return will Return a true or false Depending on the data the User put in.
     */
    private boolean areInputDataValid() {
        if (this.datePicker.getValue() != null) {
            try {
                DateConverter.convertLocalDateToString(this.datePicker.getValue());
            } catch (Exception exception) {
                return false;
            }
        }else{
            return false;
        }

        return !this.textFieldFirstName.getText().isBlank() && !this.textFieldSurname.getText().isBlank() &&
                !this.textFieldCareLevel.getText().isBlank() && !this.textFieldRoomNumber.getText().isBlank();
    }

    /**
     * Logic for exporting a file, when clicked on "Exportieren".
     */
    @FXML
    private void handleExport() {
        Patient selectedPatient = this.tableView.getSelectionModel().getSelectedItem();
        if (selectedPatient == null)
            return;

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Patient als CSV exportieren...");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV-Datei", "*.csv"));

        File file = fileChooser.showSaveDialog(buttonExport.getScene().getWindow());
        if (file == null)
            return;

        ResultSet patientData = null;
        try {
            patientData = this.dao.readPatientData(selectedPatient);
        }
        catch (SQLException exception) {
            exception.printStackTrace();
        }
        if (patientData == null)
            return;

        String filepath = file.getAbsolutePath();
        if (!file.getAbsolutePath().endsWith(".csv"))
            filepath += ".csv";

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(filepath));

            writer.write("\"Vorname Patient\",\"Nachname Patient\",\"Geburtsdatum Patient\"," +
                         "\"Pflegegrad\",\"Raum\",\"Aktenerstellungsdatum Patient\"," +
                         "\"Vorname Pflegekraft\",\"Nachname Pflegekraft\"," +
                         "\"Behandlungsdatum\",\"Behandlungsbeginn\",\"Behandlungsende\"," +
                         "\"Kurzbeschreibung\",\"Anmerkungen\"");
            while (patientData.next()) {
                String line = String.format("\"%s\",\"%s\",\"%s\",\"%s\"," +
                                            "\"%s\",\"%s\"," +
                                            "\"%s\",\"%s\"," +
                                            "\"%s\",\"%s\",\"%s\",\"%s\"," +
                                            "\"%s\"",
                                            patientData.getString(1), patientData.getString(2),
                                            patientData.getString(3), patientData.getString(4),
                                            patientData.getString(5), patientData.getString(6),
                                            patientData.getString(7), patientData.getString(8),
                                            patientData.getString(9), patientData.getString(10),
                                            patientData.getString(11), patientData.getString(12),
                                            patientData.getString(13));

                writer.newLine();
                writer.write(line);
            }

            writer.close();
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
