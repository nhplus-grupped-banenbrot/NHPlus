package de.hitec.nhplus.controller;

import de.hitec.nhplus.Main;
import de.hitec.nhplus.datastorage.CaregiverDao;
import de.hitec.nhplus.datastorage.DaoFactory;
import de.hitec.nhplus.datastorage.PatientDao;
import de.hitec.nhplus.datastorage.TreatmentDao;
import de.hitec.nhplus.model.Caregiver;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import de.hitec.nhplus.model.Patient;
import de.hitec.nhplus.model.Treatment;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * The <code>AllTreatmentController</code> contains the entire logic of the Treatment Window View. It determines which data is displayed and how to react to events.
 */

public class AllTreatmentController {

    @FXML
    private TableView<Treatment> tableView;

    @FXML
    private TableColumn<Treatment, Integer> columnId;

    @FXML
    private TableColumn<Treatment, Integer> columnPid;

    @FXML
    private TableColumn<Treatment, Integer> columnCid;

    @FXML
    private TableColumn<Treatment, String> columnDate;

    @FXML
    private TableColumn<Treatment, String> columnBegin;

    @FXML
    private TableColumn<Treatment, String> columnEnd;

    @FXML
    private TableColumn<Treatment, String> columnDescription;

    @FXML
    private ComboBox<String> comboBoxPatientSelection;

    @FXML
    private ComboBox<String> comboBoxCaregiverSelection;

    @FXML
    private Button buttonDelete;

    private final ObservableList<Treatment> treatments = FXCollections.observableArrayList();
    private TreatmentDao dao;
    private final ObservableList<String> patientSelection = FXCollections.observableArrayList();
    private ArrayList<Patient> patientList;
    private final ObservableList<String> caregiverSelection = FXCollections.observableArrayList();
    private ArrayList<Caregiver> caregiverList;

    /**
     * Default constructor for AllTreatmentController
     */
    public AllTreatmentController(){
    }

    /**
     * When initialize() gets called, all fields are already initialized. For example from the FXMLLoader
     * after loading an FXML-File. At this point of the lifecycle of the Controller, the fields can be accessed and
     * configured.
     */
    public void initialize() {
        readAllAndShowInTableView();
        this.createComboBoxData();
        comboBoxPatientSelection.setItems(patientSelection);
        comboBoxPatientSelection.getSelectionModel().select(0);
        comboBoxCaregiverSelection.setItems(caregiverSelection);
        comboBoxCaregiverSelection.getSelectionModel().select(0);

        this.columnId.setCellValueFactory(new PropertyValueFactory<>("tid"));
        this.columnPid.setCellValueFactory(new PropertyValueFactory<>("pid"));
        this.columnCid.setCellValueFactory(new PropertyValueFactory<>("cid"));
        this.columnDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        this.columnBegin.setCellValueFactory(new PropertyValueFactory<>("begin"));
        this.columnEnd.setCellValueFactory(new PropertyValueFactory<>("end"));
        this.columnDescription.setCellValueFactory(new PropertyValueFactory<>("description"));
        this.tableView.setItems(this.treatments);

        // Disabling the button to delete treatments as long, as no treatment was selected.
        this.buttonDelete.setDisable(true);
        this.tableView.getSelectionModel().selectedItemProperty().addListener(
                (observableValue, oldTreatment, newTreatment) ->
                        AllTreatmentController.this.buttonDelete.setDisable(newTreatment == null));
    }

    /**
     * When readAllAndShowInTableView gets called, the Tableview gets cleared and updated by all
     * data from nursingHome in treatments.
     */
    public void readAllAndShowInTableView() {
        comboBoxPatientSelection.getSelectionModel().select(0);
        comboBoxCaregiverSelection.getSelectionModel().select(0);
        this.treatments.clear();
        this.dao = DaoFactory.getDaoFactory().createTreatmentDao();
        try {
            this.treatments.addAll(dao.readAll());
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * When createComboBoxData is called, the nursingHome will get asked for all
     * surnames of all patient and caregiver to fill patientSelection and
     * caregiverSelection with all the surnames
     */
    private void createComboBoxData() {
        PatientDao patientDao = DaoFactory.getDaoFactory().createPatientDAO();
        try {
            patientList = (ArrayList<Patient>) patientDao.readAll();
            this.patientSelection.add("alle");
            for (Patient patient : patientList) {
                this.patientSelection.add(patient.getSurname());
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }

        CaregiverDao caregiverDao = DaoFactory.getDaoFactory().createCaregiverDAO();
        try {
            caregiverList = (ArrayList<Caregiver>) caregiverDao.readAll();
            this.caregiverSelection.add("alle");
            for (Caregiver caregiver : caregiverList) {
                this.caregiverSelection.add(caregiver.getSurname());
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }


    /**
     * This method handles the event fired by the Combobox after selecting the Patient or Caregiver.
     * It updates treatments to show only Treatments from either the Selected Patient, Caregiver or
     * Both from the nursingHome.
     */
    @FXML
    public void handleComboBox() {
        String selectedPatient = this.comboBoxPatientSelection.getSelectionModel().getSelectedItem();
        String selectedCaregiver = this.comboBoxCaregiverSelection.getSelectionModel().getSelectedItem();
        Caregiver caregiver = searchInListCaregiver(selectedCaregiver);
        Patient patient = searchInListPatient(selectedPatient);
        this.treatments.clear();
        this.dao = DaoFactory.getDaoFactory().createTreatmentDao();
        if (selectedPatient.equals("alle") && selectedCaregiver.equals("alle")) {
            try {
                this.treatments.addAll(this.dao.readAll());
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        }
        if (selectedCaregiver.equals("alle") && patient != null) {
            try {
                this.treatments.addAll(this.dao.readTreatmentsByPid(patient.getPid()));
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        }
        if (caregiver != null && selectedPatient.equals("alle")) {
            try {
                this.treatments.addAll(this.dao.readTreatmentsByCid(caregiver.getCid()));
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        }
        if (caregiver != null && patient != null) {
            try {
                this.treatments.addAll(this.dao.readTreatmentByCidAndPid(patient.getPid(), caregiver.getCid()));
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        }
    }


    /**
     * When this Method is getting called, the nursingHome is getting asked if the selected
     * Patient is in the Database, if yes it returns all Data from the Patient in patient.
     *
     * @param surname holds the surname of the selected patient in comboBoxPatient.
     * @return Will return either all Data of the Patient or null of the Patient wasn't found.
     */
    private Patient searchInListPatient(String surname) {
        for (Patient patient : this.patientList) {
            if (patient.getSurname().equals(surname)) {
                return patient;
            }
        }
        return null;
    }

    /**
     * When this Method is getting called, the nursingHome is getting asked if the selected
     * Caregiver is in the Database, if yes it returns all Data from the Patient in caregiver.
     *
     * @param surname holds the surname of the selected Caregiver in comboBoxCaregiver.
     * @return will return either all Data of the Caregiver or null of the Caregiver wasn't found.
     */
    private Caregiver searchInListCaregiver(String surname) {
        for (Caregiver caregiver : this.caregiverList) {
            if (caregiver.getSurname().equals(surname)) {
                return caregiver;
            }
        }
        return null;
    }


    /**
     * This method handles the event fired by the Button to Delete Treatments.
     * Will take the Index of the Tableview and gets the Treatment the user selected and remove it from the view.
     * Then it will delete the Treatment out of the nursingHome by using the tid.
     */
    @FXML
    public void handleDelete() {
        int index = this.tableView.getSelectionModel().getSelectedIndex();
        Treatment t = this.treatments.remove(index);
        TreatmentDao dao = DaoFactory.getDaoFactory().createTreatmentDao();
        try {
            dao.deleteById(t.getTid());
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * To be able to create a new treatment, a patient and a caregiver have to be selected via
     * comboBoxPatientSelection and comboBoxCaregiverSelection.
     * Otherwise, alert will be printed.
     */
    @FXML
    public void handleNewTreatment() {
        try {
            String selectedPatient = this.comboBoxPatientSelection.getSelectionModel().getSelectedItem();
            Patient patient = searchInListPatient(selectedPatient);
            String selectedCaregiver = this.comboBoxCaregiverSelection.getSelectionModel().getSelectedItem();
            Caregiver caregiver = searchInListCaregiver(selectedCaregiver);
            newTreatmentWindow(patient, caregiver);
            readAllAndShowInTableView();
        } catch (NullPointerException exception) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information");
            alert.setHeaderText("Patient oder Pfleger für die Behandlung fehlt!");
            alert.setContentText("Wählen Sie über die Combobox einen Patienten und einen Pfleger aus!");
            alert.showAndWait();
        }
    }

    /**
     * This method handles the event when the user clicks on the Tableview and will count the amount of clicks
     * after 2 clicks on the same index of the user it will call the method treatmentWindow
     */
    @FXML
    public void handleMouseClick() {
        tableView.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2 && (tableView.getSelectionModel().getSelectedItem() != null)) {
                int index = this.tableView.getSelectionModel().getSelectedIndex();
                Treatment treatment = this.treatments.get(index);
                treatmentWindow(treatment);
            }
        });
    }

    /**
     * When creating a new treatment, the patient and the caregiver are displayed, to see what caregiver is taking care
     * of what patient. To create the new treatment, you have to set the beginning, ending and description of the
     * treatment. The ending must not be earlier than the beginning.
     *
     * @param patient   Selected patient
     * @param caregiver Selected caregiver
     */
    public void newTreatmentWindow(Patient patient, Caregiver caregiver) {
        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("/de/hitec/nhplus/NewTreatmentView.fxml"));
            AnchorPane pane = loader.load();
            Scene scene = new Scene(pane);

            // the primary stage should stay in the background
            Stage stage = new Stage();

            NewTreatmentController controller = loader.getController();
            controller.initialize(this, stage, patient, caregiver);

            stage.setScene(scene);
            stage.setResizable(false);
            stage.showAndWait();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * When this method gets called it will load the FXML by the FXMLLoader to show an already existing Treatment
     * and will wait until the user is closing the Window
     *
     * @param treatment holds all the information of the Treatment the User wants to open
     */
    public void treatmentWindow(Treatment treatment) {
        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("/de/hitec/nhplus/TreatmentView.fxml"));
            AnchorPane pane = loader.load();
            Scene scene = new Scene(pane);

            // the primary stage should stay in the background
            Stage stage = new Stage();
            TreatmentController controller = loader.getController();
            controller.initializeController(this, stage, treatment);

            stage.setScene(scene);
            stage.setResizable(false);
            stage.showAndWait();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
