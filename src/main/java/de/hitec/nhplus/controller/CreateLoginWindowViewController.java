package de.hitec.nhplus.controller;

import de.hitec.nhplus.datastorage.Dao;
import de.hitec.nhplus.datastorage.DaoFactory;
import de.hitec.nhplus.datastorage.UserDao;
import de.hitec.nhplus.model.Treatment;
import de.hitec.nhplus.model.User;
import de.hitec.nhplus.utils.DateConverter;
import de.hitec.nhplus.utils.PasswordHasher;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Objects;

/**
 * The <code>CreateLoginWindowViewController</code> contains the entire logic of the Create Login Window. It determines which data is displayed and how to react to events.
 */

public class CreateLoginWindowViewController {

    @FXML
    private TextField textFieldUserName;

    @FXML
    private PasswordField passwordFieldPassword;

    @FXML
    private Button buttonCreate;

    @FXML
    private Button buttonCancel;

    @FXML
    private ChoiceBox<Integer> choiceBoxAccessLevel;
    private final ObservableList<Integer> accessLevels = FXCollections.observableArrayList();

    /**
     * Default constructor for CreatLoginWindowViewController
     */
    public CreateLoginWindowViewController(){}

    /**
     * Initializes and creates all text fields and buttons in the CreateLoginWindowView
     */
    public void initialize() {
        createChoiceBoxData();
        this.buttonCreate.setDisable(true);
        ChangeListener<String> inputNewUserDataListener = (observableValue, oldText, newText) ->
                buttonCreate.setDisable(areInputDataInvalid());
        this.textFieldUserName.textProperty().addListener(inputNewUserDataListener);
        this.passwordFieldPassword.textProperty().addListener(inputNewUserDataListener);

        this.choiceBoxAccessLevel.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                buttonCreate.setDisable(areInputDataInvalid());
            }
        });
    }

    /**
     * Creates the start-up window when the program is started.
     * @throws IOException Throws an exception if the .fxml isn't available
     */
    public void startUpCreateLoginView() throws IOException {
        Parent parent = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/de/hitec/nhplus/CreateLoginWindowView.fxml")));
        Stage primaryStage = new Stage();
        Scene scene = new Scene(parent);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Login Credentials");
        primaryStage.show();
    }

    /**
     * When the button "Erstellen" is clicked, the method will call createUser and creates a new user
     * with its userName, password and accessLevel.
     */
    @FXML
    private void handleCreate() {
        String userName = textFieldUserName.getText();
        String password = passwordFieldPassword.getText();
        int accessLevel = choiceBoxAccessLevel.getValue();
        try {
            User user = new User(userName, PasswordHasher.getHashedPassword(password), accessLevel);
            createUser(user);
            Stage stage = (Stage) buttonCreate.getScene().getWindow();
            stage.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates the user after handleCreate is being clicked
     * @param user User gets created
     */
    private void createUser(User user) {
        UserDao userDao = DaoFactory.getDaoFactory().createUserDAO();
        try {
            userDao.create(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Closes the window, if "Abbrechen" is clicked
     */
    @FXML
    private void handleButtonCancel() {
        Stage stage = (Stage) buttonCancel.getScene().getWindow();
        stage.close();
    }

    /**
     * Lets you choose an Access Level
     */
    private void createChoiceBoxData() {
        accessLevels.addAll(1, 2);
        choiceBoxAccessLevel.setItems(accessLevels);
    }

    /**
     * Checks if input data is not valid
     * @return Gives back invalid data
     */
    private boolean areInputDataInvalid() {
        return textFieldUserName.getText().isBlank() ||
               passwordFieldPassword.getText().isEmpty() ||
               choiceBoxAccessLevel.getSelectionModel().getSelectedIndex() == -1;
    }
}
