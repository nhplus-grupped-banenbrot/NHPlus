package de.hitec.nhplus.controller;

import de.hitec.nhplus.Main;
import de.hitec.nhplus.model.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.stage.Window;

import java.io.IOException;

/**
 * The <code>MainWindowController</code> contains logic of the Main Window. It determines which data is displayed and how to react to events.
 */

public class MainWindowController {

    @FXML
    private BorderPane mainBorderPane;

    @FXML
    private Button buttonCreateLogin;

    /**
     * Default constructor for MainWindowController
     */
    public MainWindowController(){}

    /**
     * Initializes the current user and shows or hides the "Login erstellen",
     * if the Access Level isn't the expected one.
     */
    public void initialize() {
        User currentUser = LoginWindowController.getCurrentUser();
        buttonCreateLogin.setVisible(currentUser != null && currentUser.getAccessLevel() == 2);
    }

    /**
     * Shows all patients if the "Patienten" is clicked
     */
    @FXML
    private void handleShowAllPatient() {
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/de/hitec/nhplus/AllPatientView.fxml"));
        try {
            mainBorderPane.setCenter(loader.load());
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
    /**
     * Shows all treatments if the "Behandlungen" is clicked
     */
    @FXML
    private void handleShowAllTreatments() {
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/de/hitec/nhplus/AllTreatmentView.fxml"));
        try {
            mainBorderPane.setCenter(loader.load());
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
    /**
     * Shows all caregiver if the "Pflegekräfte" is clicked
     */
    @FXML
    private void handleShowAllCaregivers() {
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/de/hitec/nhplus/AllCaregiverView.fxml"));
        try {
            mainBorderPane.setCenter(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the LoginWindowView, if the user has got access to it
     * @throws IOException Throws an exception if the .fxml isn't available
     */
    @FXML
    private void handleCreateLogin() throws IOException {
        CreateLoginWindowViewController createLoginWindowViewController = new CreateLoginWindowViewController();
        createLoginWindowViewController.startUpCreateLoginView();
    }
}
