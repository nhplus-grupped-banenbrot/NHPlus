package de.hitec.nhplus.controller;

import de.hitec.nhplus.datastorage.DaoFactory;
import de.hitec.nhplus.datastorage.CaregiverDao;
import de.hitec.nhplus.datastorage.TreatmentDao;
import de.hitec.nhplus.model.Treatment;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import de.hitec.nhplus.model.Caregiver;
import de.hitec.nhplus.utils.DateConverter;

import java.sql.SQLException;
import java.time.LocalDate;



/**
 * The AllCaregiverController contains the entire logic of the caregiver view. It determines which data is displayed and how to react to events.
 */
public class AllCaregiverController {

    @FXML
    private TableView<Caregiver> tableView;

    @FXML
    private TableColumn<Caregiver, Integer> colID;

    @FXML
    private TableColumn<Caregiver, String> colFirstName;

    @FXML
    private TableColumn<Caregiver, String> colSurname;

    @FXML
    private TableColumn<Caregiver, String> colTelephone;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnAdd;

    @FXML
    private TextField txfSurname;

    @FXML
    private TextField txfFirstname;

    @FXML
    private TextField txfTelephone;

    private final ObservableList<Caregiver> caregivers = FXCollections.observableArrayList();
    private final ObservableList<Treatment> treatments = FXCollections.observableArrayList();
    private CaregiverDao caregiverDao;
    private TreatmentDao treatmentDao;

    /**
     * Default constructor for AllCaregiverController
     */
    public AllCaregiverController(){}

    /**
     * When initialize() gets called, all fields are already initialized. For example from the FXMLLoader
     * after loading an FXML-File. At this point of the lifecycle of the Controller, the fields can be accessed and
     * configured.
     */
    public void initialize() {
        this.readAllAndShowInTableView();

        this.colID.setCellValueFactory(new PropertyValueFactory<>("cid"));

        // CellValueFactory to show property values in TableView
        this.colFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        // CellFactory to write property values from with in the TableView
        this.colFirstName.setCellFactory(TextFieldTableCell.forTableColumn());

        this.colSurname.setCellValueFactory(new PropertyValueFactory<>("surname"));
        this.colSurname.setCellFactory(TextFieldTableCell.forTableColumn());

        this.colTelephone.setCellValueFactory(new PropertyValueFactory<>("telephoneNumber"));
        this.colTelephone.setCellFactory(TextFieldTableCell.forTableColumn());

        //Anzeigen der Daten
        this.tableView.setItems(this.caregivers);

        this.btnDelete.setDisable(true);
        this.tableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Caregiver>() {
            @Override
            public void changed(ObservableValue<? extends Caregiver> observableValue, Caregiver oldCaregiver, Caregiver newCaregiver) {;
                AllCaregiverController.this.btnDelete.setDisable(newCaregiver == null);
            }
        });

        this.btnAdd.setDisable(true);
        ChangeListener<String> inputNewCaregiverListener = (observableValue, oldText, newText) ->
                AllCaregiverController.this.btnAdd.setDisable(!AllCaregiverController.this.areInputDataValid());
        this.txfSurname.textProperty().addListener(inputNewCaregiverListener);
        this.txfFirstname.textProperty().addListener(inputNewCaregiverListener);
        this.txfTelephone.textProperty().addListener(inputNewCaregiverListener);
    }

    /**
     * When a cell of the column with first names was changed, this method will be called, to persist the change.
     *
     * @param event Event including the changed object and the change.
     */
    @FXML
    public void handleOnEditFirstname(TableColumn.CellEditEvent<Caregiver, String> event) {
        event.getRowValue().setFirstName(event.getNewValue());
        this.doUpdate(event);
    }

    /**
     * When a cell of the column with surnames was changed, this method will be called, to persist the change.
     *
     * @param event Event including the changed object and the change.
     */
    @FXML
    public void handleOnEditSurname(TableColumn.CellEditEvent<Caregiver, String> event) {
        event.getRowValue().setSurname(event.getNewValue());
        this.doUpdate(event);
    }

    /**
     * When a cell of the column with care levels was changed, this method will be called, to persist the change.
     *
     * @param event Event including the changed object and the change.
     */
    @FXML
    public void handleOnEditTelephoneNumber(TableColumn.CellEditEvent<Caregiver, String> event) {
        event.getRowValue().setTelephoneNumber(event.getNewValue());
        this.doUpdate(event);
    }

    /**
     * Updates a caregiver by calling the method update of {@link CaregiverDao}.
     *
     * @param event Event including the changed object and the change.
     */
    private void doUpdate(TableColumn.CellEditEvent<Caregiver, String> event) {
        try {
            this.caregiverDao.update(event.getRowValue());
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Reloads all caregivers to the table by clearing the list of all caregivers and filling it again by all persisted
     * caregivers, delivered by {@link CaregiverDao}.
     */
    private void readAllAndShowInTableView() {
        this.caregivers.clear();
        this.caregiverDao = DaoFactory.getDaoFactory().createCaregiverDAO();
        try {
            this.caregivers.addAll(this.caregiverDao.readAll());
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * This method handles events fired by the button to delete caregivers. It calls {@link CaregiverDao} to delete the
     * caregiver from the database and removes the object from the list, which is the data source of the
     * TableView.
     */
    @FXML
    public void handleDelete() {
        boolean isDeletable = true;
        Caregiver selectedItem = this.tableView.getSelectionModel().getSelectedItem();
        if(selectedItem != null) {
           treatmentArrayList(selectedItem.getCid());
            for (Treatment treatment : treatments) {
                if (isDeleteNotPossible(treatment)) {
                    isDeletable = false;
                    blockCareGiver(selectedItem);
                    readAllAndShowInTableView();
                    break;
                }
            }
            if(isDeletable) {
                try {
                    DaoFactory.getDaoFactory().createCaregiverDAO().deleteById(selectedItem.getCid());
                    this.tableView.getItems().remove(selectedItem);
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
            }
        }
    }

    /**
     * Adds all treatments read by their cid from SQL to an ArrayList
     * @param cid Refers to the Caregiver ID
     */
    private void treatmentArrayList(long cid){
        treatmentDao = DaoFactory.getDaoFactory().createTreatmentDao();
        try{
            this.treatments.addAll(this.treatmentDao.readTreatmentsByCid(cid));
        }catch (SQLException sqlException){
            sqlException.printStackTrace();
        }

    }

    /**
     * When a caregiver can't be deleted, a pop-up appears asking if the caregiver should be blocked instead.
     *
     * @param caregiver Caregiver from the selected treatment
     */
    private void blockCareGiver(Caregiver caregiver) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Löschen nicht möglich");
        alert.setHeaderText("Das Löschen dieser Daten ist nicht möglich");
        alert.setContentText("Möchten Sie die Daten blockieren?");
        ButtonType buttonBlock = new ButtonType("Blockieren");
        ButtonType buttonCancel = new ButtonType("Abbrechen", ButtonBar.ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonBlock, buttonCancel);
        alert.showAndWait().ifPresent(response -> {
            if (response == buttonBlock) {
                caregiverDao.blockCaregiver(caregiver.getCid());
            }
        });
    }

    /**
     * Checks, if the treatment can be deleted by adding ten years of the date the treatment was created
     * @param treatment Is used to get the date when the treatment was created
     * @return Gives back the year the treatment was created + ten years
     */
    private boolean isDeleteNotPossible(Treatment treatment){
        LocalDate treatmentCreated = DateConverter.convertStringToLocalDate(treatment.getDate());
        LocalDate treatmentCreatedPlusTenYears = treatmentCreated.plusYears(10);
        return LocalDate.now().isBefore(treatmentCreatedPlusTenYears);
    }

    /**
     * This method handles the events fired by the button to add a caregiver. It collects the data from the
     * TextField, creates an object of class Caregiver of it and passes the object to
     * {@link CaregiverDao} to persist the data.
     */
    @FXML
    public void handleAdd() {
        String surname = this.txfSurname.getText();
        String firstName = this.txfFirstname.getText();
        String telephoneNumber = this.txfTelephone.getText();
        try {
            this.caregiverDao.create(new Caregiver(firstName, surname, telephoneNumber, 0));
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        readAllAndShowInTableView();
        clearTextfields();
    }

    /**
     * Clears all contents from all TextField.
     */
    private void clearTextfields() {
        this.txfFirstname.clear();
        this.txfSurname.clear();
        this.txfTelephone.clear();
    }

    /**
     * Checks if the data input is valid
     * @return Gives back fields that are not blank
     */
    private boolean areInputDataValid() {
        return !this.txfFirstname.getText().isBlank() && !this.txfSurname.getText().isBlank() &&
               !this.txfTelephone.getText().isBlank();
    }
}
