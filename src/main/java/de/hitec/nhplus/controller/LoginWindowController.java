package de.hitec.nhplus.controller;

import de.hitec.nhplus.datastorage.DaoFactory;
import de.hitec.nhplus.datastorage.UserDao;
import de.hitec.nhplus.model.User;
import de.hitec.nhplus.utils.PasswordHasher;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * The <code>LoginWindowController</code> contains the entire logic of the Login Window. It determines which data is displayed and how to react to events.
 */

public class LoginWindowController {

    @FXML
    private TextField txfUsername;
    @FXML
    private PasswordField pwfPassword;
    @FXML
    private Button loginButton;
    private UserDao userDao;
    /**
     * User
     */
    public static User currentUser;

    /**
     * Default constructor for LoginWindowController
     */
    public LoginWindowController(){}

    /**
     * When a user is successfully logging in, this method opens the MainWindowView
     *
     * @param user User that's logged in
     * @throws IOException Throws an exception if the .fxml isn't available
     */
    private void handleSuccessfulLogin(User user) throws IOException {
        LoginWindowController.setCurrentUser(user);
        Window currentWindow = loginButton.getScene().getWindow();

        Parent parent = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/de/hitec/nhplus/MainWindowView.fxml")));
        Stage primaryStage = new Stage();
        Scene scene = new Scene(parent);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Willkommen auf NHPlus");
        primaryStage.show();

        currentWindow.hide();
    }

    /**
     * If a login is not successful, this method shows a pop-up that a login with that data couldn't be found
     */
    private void handleLoginFailure() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Login nicht erfolgreich");
        alert.setHeaderText("Konnte kein Konto mit den eingegebenen Benutzernamen und Passwort finden");
        ButtonType buttonOK = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);

        alert.getButtonTypes().setAll(buttonOK);
        alert.showAndWait();
    }

    /**
     * This method will call the method handleSuccessfulLogin or handleLoginFailure, depending on the users input
     *
     * @throws IOException Reference for the handleSuccessfulLogin method
     */
    @FXML
    public void handleLogin() throws IOException {
        this.userDao = DaoFactory.getDaoFactory().createUserDAO();
        List<User> users = null;

        try {
            String passwordHash = PasswordHasher.getHashedPassword(pwfPassword.getText());
            users = this.userDao.readUsersByCredentials(txfUsername.getText(), passwordHash);
            assert users.size() <= 1;
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        if (users != null && users.size() == 1)
            handleSuccessfulLogin(users.get(0));
        else
            handleLoginFailure();
    }

    /**
     * Sets and gets the current user
     * @param user Current user
     */
    private static void setCurrentUser(User user) {
        LoginWindowController.currentUser = user;
    }

    /**
     * Gives out the current user
     * @return User that logged in
     */
    public static User getCurrentUser() {
        return LoginWindowController.currentUser;
    }
}
