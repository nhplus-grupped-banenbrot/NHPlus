package de.hitec.nhplus.controller;

import de.hitec.nhplus.datastorage.CaregiverDao;
import de.hitec.nhplus.datastorage.DaoFactory;
import de.hitec.nhplus.datastorage.PatientDao;
import de.hitec.nhplus.datastorage.TreatmentDao;
import de.hitec.nhplus.model.Caregiver;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import de.hitec.nhplus.model.Patient;
import de.hitec.nhplus.model.Treatment;
import de.hitec.nhplus.utils.DateConverter;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
/**
 * The <code>TreatmentController</code> contains logic of the Treatment view. It determines which data is displayed and how to react to events.
 */
public class TreatmentController {

    @FXML
    private Label labelPatientName;

    @FXML
    private Label labelCareLevel;

    @FXML
    private Label labelPhoneNumber;

    @FXML
    private Label labelCaregiverName;

    @FXML
    private TextField textFieldBegin;

    @FXML
    private TextField textFieldEnd;

    @FXML
    private TextField textFieldDescription;

    @FXML
    private TextArea textAreaRemarks;

    @FXML
    private DatePicker datePicker;

    private AllTreatmentController controller;
    private Stage stage;
    private Patient patient;
    private Treatment treatment;
    private Caregiver caregiver;

    /**
     * Default constructor for TreatmentController
     */
    public  TreatmentController(){}

    /**
     * This method initializes the {@link AllTreatmentController} and shows data of it.
     * @param controller Indicates at which Controller it refers to
     * @param stage Indicates the opened window/pop up
     * @param treatment Indicates at which treatment it looks / it opens
     */
    public void initializeController(AllTreatmentController controller, Stage stage, Treatment treatment) {
        this.stage = stage;
        this.controller= controller;
        PatientDao pDao = DaoFactory.getDaoFactory().createPatientDAO();
        CaregiverDao cDao = DaoFactory.getDaoFactory().createCaregiverDAO();
        try {
            this.patient = pDao.read((int) treatment.getPid());
            this.caregiver = cDao.read((int) treatment.getCid());
            this.treatment = treatment;
            showData();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Shows all data from a created treatment, when double-clicking / opening a treatment.
     */
    private void showData(){
        this.labelPatientName.setText(patient.getSurname()+", "+patient.getFirstName());
        this.labelCareLevel.setText(patient.getCareLevel());
        LocalDate date = DateConverter.convertStringToLocalDate(treatment.getDate());
        this.datePicker.setValue(date);
        this.textFieldBegin.setText(this.treatment.getBegin());
        this.textFieldEnd.setText(this.treatment.getEnd());
        this.textFieldDescription.setText(this.treatment.getDescription());
        this.textAreaRemarks.setText(this.treatment.getRemarks());
        this.labelCaregiverName.setText(caregiver.getSurname() + ", " + caregiver.getFirstName() + " (" + caregiver.getCid() + ")");
        this.labelPhoneNumber.setText(caregiver.getTelephoneNumber());
    }

    /**
     * When changing any data from a treatment, this method calls doUpdate and
     * refreshes the AllTreatmentView by calling the
     * method controller.readAllAndShowInTableView.
     */
    @FXML
    public void handleChange(){
        String formattedDatePickerValue = this.datePicker.getValue().format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        this.treatment.setDate(formattedDatePickerValue);
        this.treatment.setBegin(textFieldBegin.getText());
        this.treatment.setEnd(textFieldEnd.getText());
        this.treatment.setDescription(textFieldDescription.getText());
        this.treatment.setRemarks(textAreaRemarks.getText());
        doUpdate();
        controller.readAllAndShowInTableView();
        stage.close();
    }

    /**
     * Updates the treatment after changes have been done and the User clicked the button "Ändern".
     */
    private void doUpdate(){
        TreatmentDao dao = DaoFactory.getDaoFactory().createTreatmentDao();
        try {
            dao.update(treatment);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Closes window if the Abbruch is clicked
     */
    @FXML
    public void handleCancel(){
        stage.close();
    }
}