package de.hitec.nhplus.model;

import javafx.beans.property.SimpleStringProperty;

/**
 * The class Person is an abstract class to handle common attributes for derived classes.
 */
public abstract class Person {
    private final SimpleStringProperty firstName;
    private final SimpleStringProperty surname;

    /**
     * Constructor to initiate an object of class Person with the given parameter.
     *
     * @param firstName holds the firstname of the Person
     * @param surname holds the surname of the Person
     */
    public Person(String firstName, String surname) {
        this.firstName = new SimpleStringProperty(firstName);
        this.surname = new SimpleStringProperty(surname);
    }

    /**
     * When this method is called return the Value of firstName
     *
     * @return Returns the firstName of the asked Person
     */
    public String getFirstName() {
        return firstName.get();
    }

    /**
     * When this method is called return the Value of surname
     *
     * @return Returns the Surname of the asked Person
     */
    public String getSurname() {
        return surname.get();
    }

    /**
     * when this method gets called sets firstName for the Person
     *
     * @param firstName holds the firstName of the Person
     */
    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    /**
     * when this mathod gets called sets surname for the Person
     *
     * @param surname holds the surname of the Person
     */
    public void setSurname(String surname) {
        this.surname.set(surname);
    }
}
