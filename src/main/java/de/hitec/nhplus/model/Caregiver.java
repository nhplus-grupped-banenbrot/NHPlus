package de.hitec.nhplus.model;

import de.hitec.nhplus.utils.DateConverter;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Caregivers work in a NURSING home and treat patients.
 */
public class Caregiver extends Person {
    private SimpleLongProperty cid;
    private final SimpleStringProperty telephoneNumber;
    private SimpleIntegerProperty isLocked;
    private final List<Treatment> allTreatments = new ArrayList<>();

    /**
     * Constructor to initiate an object of class Caregiver with the given parameter. Use this constructor
     * to initiate objects, which are not persisted yet, because it will not have a caregiver id (cid).
     *
     * @param firstName First name of the caregiver.
     * @param surname Last name of the caregiver.
     * @param telephoneNumber Telephone number of the caregiver.
     * @param isLocked Whether the databank entry of the caregiver is locked.
     */
    public Caregiver(String firstName, String surname, String telephoneNumber, int isLocked) {
        super(firstName, surname);
        this.telephoneNumber = new SimpleStringProperty(telephoneNumber);
        this.isLocked = new SimpleIntegerProperty(isLocked);
    }

    /**
     * Constructor to initiate an object of class Caregiver with the given parameter. Use this constructor
     * to initiate objects, which are already persisted and have a caregiver id (cid).
     *
     * @param cid Caregiver id.
     * @param firstName First name of the caregiver.
     * @param surname Last name of the caregiver.
     * @param telephoneNumber Telephone number of the caregiver.
     * @param isLocked Whether the databank entry of the caregiver is locked.
     */
    public Caregiver(long cid, String firstName, String surname, String telephoneNumber, int isLocked) {
        super(firstName, surname);
        this.cid = new SimpleLongProperty(cid);
        this.telephoneNumber = new SimpleStringProperty(telephoneNumber);
        this.isLocked = new SimpleIntegerProperty(isLocked);
    }

    /**
     * When this Method is called returns the Value of cid
     *
     * @return Returns the cid from the Selected Caregiver.
     */
    public long getCid() {
        return cid.get();
    }

    /**
     * When this Method is called returns the value of telephoneNumber
     *
     * @return Returns the telephoneNumber from the Selected Caregiver
     */
    public String getTelephoneNumber() {
        return telephoneNumber.get();
    }

    /**
     * when this method is called returns the value of isLocked
     *
     * @return Returns the isLocked Status of the Selected Caregiver.
     */
    public int getIsLocked() {
        return isLocked.get();
    }

    /**
     * when this method is called sets the telephoneNumber for the Selected Caregiver.
     *
     * @param telephoneNumber holds the Value of the telephoneNumber
     */
    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber.set(telephoneNumber);
    }

    /**
     * Returns a string with the Caregiver information,
     * such as cid, firstName, surname, telephoneNumber and islocked.
     *
     * @return Returns the string with Caregiver and all its information
     */
    public String toString() {
        return "Caregiver" + "\nCID: " + this.cid +
               "\nFirstname: " + this.getFirstName() +
               "\nSurname: " + this.getSurname() +
               "\nTelephoneNumber: " + this.telephoneNumber +
               "\nisLocked: " + this.isLocked +
               "\n";
    }
}