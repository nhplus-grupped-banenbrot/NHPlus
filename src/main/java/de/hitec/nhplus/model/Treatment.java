package de.hitec.nhplus.model;

import de.hitec.nhplus.utils.DateConverter;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Treatments are happening in a NURSING home and are done by nurses
 */
public class Treatment {
    private SimpleLongProperty tid;
    private final SimpleLongProperty pid;
    private final SimpleLongProperty cid;
    private final SimpleStringProperty date;
    private final SimpleStringProperty begin;
    private final SimpleStringProperty end;
    private final SimpleStringProperty description;
    private final SimpleStringProperty remarks;

    /**
     * Constructor to initiate an object of class Treatment with the given parameter. Use this constructor
     * to initiate objects, which are not persisted yet, because it will not have a treatment id (tid).
     *
     * @param pid Id of the treated patient.
     * @param cid Id of the treated caregiver.
     * @param date Date of the Treatment.
     * @param begin Time of the start of the treatment in format "hh:MM"
     * @param end Time of the end of the treatment in format "hh:MM".
     * @param description Description of the treatment.
     * @param remarks Remarks to the treatment.
     */
    public Treatment(long pid, long cid, LocalDate date, LocalTime begin,
                     LocalTime end, String description, String remarks) {
        this.pid = new SimpleLongProperty(pid);
        this.cid = new SimpleLongProperty(cid);
        this.date = new SimpleStringProperty(DateConverter.convertLocalDateToString(date));
        this.begin = new SimpleStringProperty(DateConverter.convertLocalTimeToString(begin));
        this.end = new SimpleStringProperty(DateConverter.convertLocalTimeToString(end));
        this.description = new SimpleStringProperty(description);
        this.remarks = new SimpleStringProperty(remarks);
    }

    /**
     * Constructor to initiate an object of class Treatment with the given parameter. Use this constructor
     * to initiate objects, which are already persisted and have a treatment id (tid).
     *
     * @param tid Id of the treatment.
     * @param pid Id of the treated patient.
     * @param cid Id of the treated caregiver.
     * @param date Date of the Treatment.
     * @param begin Time of the start of the treatment in format "hh:MM"
     * @param end Time of the end of the treatment in format "hh:MM".
     * @param description Description of the treatment.
     * @param remarks Remarks to the treatment.
     */
    public Treatment(long tid, long pid,long cid, LocalDate date, LocalTime begin,
                     LocalTime end, String description, String remarks) {
        this.tid = new SimpleLongProperty(tid);
        this.pid = new SimpleLongProperty(pid);
        this.cid = new SimpleLongProperty(cid);
        this.date = new SimpleStringProperty(DateConverter.convertLocalDateToString(date));
        this.begin = new SimpleStringProperty(DateConverter.convertLocalTimeToString(begin));
        this.end = new SimpleStringProperty(DateConverter.convertLocalTimeToString(end));
        this.description = new SimpleStringProperty(description);
        this.remarks = new SimpleStringProperty(remarks);
    }

    /**
     * When this method is called return the Value of tid
     *
     * @return Returns tid from the asked Treatment
     */
    public long getTid() {
        return tid.get();
    }

    /**
     * When this method is called return the Value of pid
     *
     * @return Returns pid from the asked Treatment
     */
    public long getPid() {
        return this.pid.get();
    }

    /**
     * When this method is called return the Value of cid
     *
     * @return Returns cid from the asked Treatment
     */
    public long getCid() { return cid.get(); }

    /**
     * When this method is called return the Value of date
     *
     * @return Returns date from the asked Treatment
     */
    public String getDate() {
        return date.get();
    }

    /**
     * When this method is called return the Value of begin
     *
     * @return Returns Begin from the asked Treatment
     */
    public String getBegin() {
        return begin.get();
    }

    /**
     * When this method is called return the Value of end
     *
     * @return Returns end from the asked Treatment
     */
    public String getEnd() {
        return end.get();
    }

    /**
     * When this method is called return the Value of description
     *
     * @return Returns description from the asked Treatment
     */
    public String getDescription() {
        return description.get();
    }

    /**
     * When this method is called return the Value of remarks
     *
     * @return Returns remarks from the asked Treatment
     */
    public String getRemarks() {
        return remarks.get();
    }

    /**
     * When this methode gets called it sets the date as the given Value
     *
     * @param date holds the value of the date the User Selected
     */
    public void setDate(String date) {
        this.date.set(date);
    }

    /**
     * When this methode gets called it sets the begin for the Treatment
     *
     * @param begin holds the value of the begin the user Selected
     */
    public void setBegin(String begin) {
        this.begin.set(begin);;
    }

    /**
     * when this method gets called it sets the end for the Treatment
     *
     * @param end holds the value of the end the user Selected
     */
    public void setEnd(String end) {
        this.end.set(end);;
    }

    /**
     * when this method gets called it sets description for the Treatment
     *
     * @param description holds the description the User provided
     */
    public void setDescription(String description) {
        this.description.set(description);
    }

    /**
     * when this method gets called it sets remarks for the Treatment
     *
     * @param remarks holds the remarks the User provided
     */
    public void setRemarks(String remarks) {
        this.remarks.set(remarks);
    }


    /**
     * Returns a string with the Treatment information,
     * such as tid, cid, pid, date, begin, end, description and remarks.
     *
     * @return Returns the string with treatment and all its information
     */
    public String toString() {
        return "\nBehandlung" + "\nTID: " + this.tid +
                "\nCID: " + this.cid +
                "\nPID: " + this.pid +
                "\nDate: " + this.date +
                "\nBegin: " + this.begin +
                "\nEnd: " + this.end +
                "\nDescription: " + this.description +
                "\nRemarks: " + this.remarks + "\n";
    }
}
