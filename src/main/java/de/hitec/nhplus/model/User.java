package de.hitec.nhplus.model;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Users have access to NHPlus in order to maintain all treatments, caregivers, and patients.
 */
public class User {
    private SimpleLongProperty uid;
    private final SimpleStringProperty username;
    private final SimpleStringProperty passwordHash;
    private SimpleLongProperty accessLevel;

    /**
     * Constructor to initiate an object of class <code>User</code> with the given parameter. Use this constructor
     * to initiate objects, which are not persisted yet, because it will not have a user id (uid).
     *
     * @param username Username of the user.
     * @param passwordHash Hash of the password of the user.
     * @param accessLevel Access level of the user.
     */
    public User(String username, String passwordHash, long accessLevel) {
        this.username = new SimpleStringProperty(username);
        this.passwordHash = new SimpleStringProperty(passwordHash);
        this.accessLevel = new SimpleLongProperty(accessLevel);
    }

    /**
     * Constructor to initiate an object of class User with the given parameter. Use this constructor
     * to initiate objects, which are already persisted and have a user id (uid).
     *
     * @param uid User id.
     * @param username Username of the user.
     * @param passwordHash Hash of the password of the user.
     * @param accessLevel Access level of the user.
     */
    public User(long uid, String username, String passwordHash, long accessLevel) {
        this.uid = new SimpleLongProperty(uid);
        this.username = new SimpleStringProperty(username);
        this.passwordHash = new SimpleStringProperty(passwordHash);
        this.accessLevel = new SimpleLongProperty(accessLevel);
    }

    /**
     * When this method is called return the Value of uid
     *
     * @return Returns uid from the asked User
     */
    public long getUid() {
        return uid.get();
    }

    /**
     * When this method is called return the Value of username
     *
     * @return Returns username from the asked User
     */
    public String getUsername() {
        return username.get();
    }

    /**
     * When this method is called return the Value of passwordHash
     *
     * @return Returns passwordHash from the asked User
     */
    public String getPasswordHash() {
        return passwordHash.get();
    }

    /**
     * When this method is called return the Value of accessLevel
     *
     * @return Returns accessLevel from the asked User
     */
    public long getAccessLevel() {
        return accessLevel.get();
    }

    /**
     * Returns a string with the user information, except for the password hash,
     * such as the username, or accessLevel
     *
     * @return A string describing the user
     */
    public String toString() {
        return "User" + "\nUID: " + this.uid +
               "\nUsername: " + this.getUsername() +
               "\naccessLevel: " + this.getAccessLevel() +
               "\n";
    }
}