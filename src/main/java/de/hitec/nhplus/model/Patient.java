package de.hitec.nhplus.model;

import de.hitec.nhplus.utils.DateConverter;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Patients live in a NURSING home and are treated by nurses.
 */
public class Patient extends Person {
    private SimpleLongProperty pid;
    private final SimpleStringProperty dateOfBirth;
    private final SimpleStringProperty careLevel;
    private final SimpleStringProperty roomNumber;
    private final SimpleStringProperty createdOn;
    private final SimpleIntegerProperty isLocked;
    private final List<Treatment> allTreatments = new ArrayList<>();

    /**
     * Constructor to initiate an object of class Patient with the given parameter. Use this constructor
     * to initiate objects, which are not persisted yet, because it will not have a patient id (pid).
     *
     * @param firstName   First name of the patient.
     * @param surname     Last name of the patient.
     * @param dateOfBirth Date of birth of the patient.
     * @param careLevel   Care level of the patient.
     * @param roomNumber  Room number of the patient.
     * @param createdOn   Date when patient was created.
     * @param isLocked    Blocked status of the patient.
     */
    public Patient(String firstName, String surname, LocalDate dateOfBirth, String careLevel, String roomNumber, LocalDate createdOn, int isLocked) {
        super(firstName, surname);
        this.dateOfBirth = new SimpleStringProperty(DateConverter.convertLocalDateToString(dateOfBirth));
        this.careLevel = new SimpleStringProperty(careLevel);
        this.roomNumber = new SimpleStringProperty(roomNumber);
        this.createdOn = new SimpleStringProperty(DateConverter.convertLocalDateToString(createdOn));
        this.isLocked = new SimpleIntegerProperty(isLocked);
    }

    /**
     * Constructor to initiate an object of class Patient with the given parameter. Use this constructor
     * to initiate objects, which are already persisted and have a patient id (pid).
     *
     * @param pid         Patient id.
     * @param firstName   First name of the patient.
     * @param surname     Last name of the patient.
     * @param dateOfBirth Date of birth of the patient.
     * @param careLevel   Care level of the patient.
     * @param roomNumber  Room number of the patient.
     * @param createdOn   Date when patient was created.
     * @param isLocked    Blocked status of the patient.
     */
    public Patient(long pid, String firstName, String surname, LocalDate dateOfBirth, String careLevel, String roomNumber, LocalDate createdOn, int isLocked) {
        super(firstName, surname);
        this.pid = new SimpleLongProperty(pid);
        this.dateOfBirth = new SimpleStringProperty(DateConverter.convertLocalDateToString(dateOfBirth));
        this.careLevel = new SimpleStringProperty(careLevel);
        this.roomNumber = new SimpleStringProperty(roomNumber);
        this.createdOn = new SimpleStringProperty(DateConverter.convertLocalDateToString(createdOn));
        this.isLocked = new SimpleIntegerProperty(isLocked);
    }

    /**
     * When this method is called return the Value of pid
     *
     * @return Returns the pid for the asked Patient
     */
    public long getPid() {
        return pid.get();
    }

    /**
     * When this method is called return the Value of dateOfBirth
     *
     * @return Returns the dateOfBith for the asked Patient
     */
    public String getDateOfBirth() {
        return dateOfBirth.get();
    }

    /**
     * When this method is called return the Value of careLevel
     *
     * @return Returns the careLevel for the asked Patient
     */
    public String getCareLevel() {
        return careLevel.get();
    }

    /**
     * When this method is called return the Value of roomNumber
     *
     * @return Returns the roomNumber for the asked Patient
     */
    public String getRoomNumber() {
        return roomNumber.get();
    }

    /**
     * When this method is called return the Value of createdOn
     *
     * @return Returns the createdOn for the asked Patient
     */
    public String getCreatedOn() {
        return createdOn.get();
    }

    /**
     * When this method is called return the Value of isLocked
     *
     * @return Returns the isLocked for the asked Patient
     */
    public int isLocked() {
        return isLocked.get();
    }


    /**
     * Stores the given string as new birthOfDate.
     *
     * @param dateOfBirth as string in the following format: YYYY-MM-DD.
     */
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth.set(dateOfBirth);
    }

    /**
     * when this Method is called set the careLevel for the Selected Patient
     *
     * @param careLevel holds the Value of the careLevel
     */
    public void setCareLevel(String careLevel) {
        this.careLevel.set(careLevel);
    }

    /**
     * when this method is called set the roomNumber for the Selected Patient
     *
     * @param roomNumber holds the Value of the roomNumber
     */
    public void setRoomNumber(String roomNumber) {
        this.roomNumber.set(roomNumber);
    }

    /**
     * Returns a string with the Patient information,
     * such as pid, firstName, surname, dateOfBirth, careLevel, roomNumber, Created on and isLocked.
     *
     * @return Returns the string with Patient and all its information
     */
    public String toString() {
        return "Patient" + "\nPID: " + this.pid +
                "\nFirstname: " + this.getFirstName() +
                "\nSurname: " + this.getSurname() +
                "\nBirthday: " + this.dateOfBirth +
                "\nCareLevel: " + this.careLevel +
                "\nRoomNumber: " + this.roomNumber +
                "\nCreated On: " + this.createdOn +
                "\nLocked: " + this.isLocked +
                "\n";
    }
}