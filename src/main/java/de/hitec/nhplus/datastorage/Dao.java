package de.hitec.nhplus.datastorage;

import java.sql.SQLException;
import java.util.List;

/**
 * The <code>Dao</code> provides logic for creating Dao's.
 * @param <T> Template Object for the different Instances of the Models
 */

public interface Dao<T> {

    /**
     * Creates the SQL Entry and throws a SQLException
     *
     * @param t Holds some Values for the Database
     * @throws SQLException Throws a SQL exception
     */
    void create(T t) throws SQLException;

    /**
     * Reads Information of the Database
     *
     * @param key holds information for the Database
     * @return Returns information
     * @throws SQLException Throws a SQL Exception
     */
    T read(long key) throws SQLException;

    /**
     * Reads all info from the Database
     *
     * @return Returns the information
     * @throws SQLException throws a Sql Exception
     */
    List<T> readAll() throws SQLException;

    /**
     * Updates information in the Database
     *
     * @param t hold the new information for the database
     * @throws SQLException throws a SQL Exception
     */
    void update(T t) throws SQLException;

    /**
     * Deletes a entry of the Database by iD
     *
     * @param key holds the ID of the target
     * @throws SQLException throws a SQL exception
     */
    void deleteById(long key) throws SQLException;
}
