package de.hitec.nhplus.datastorage;

import de.hitec.nhplus.model.Caregiver;
import de.hitec.nhplus.utils.DateConverter;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Implements the Interface DaoImp. Overrides methods to generate specific PreparedStatements,
 * to execute the specific SQL Statements.
 */
public class CaregiverDao extends DaoImp<Caregiver> {

    /**
     * The constructor initiates an object of CaregiverDao and passes the connection to its super class.
     *
     * @param connection Object of Connection to execute the SQL-statements.
     */
    public CaregiverDao(Connection connection) {
        super(connection);
    }

    /**
     * Generates a PreparedStatement to persist the given object of Caregiver.
     *
     * @param caregiver Object of Caregiver to persist.
     * @return PreparedStatement to insert the given caregiver.
     */
    @Override
    protected PreparedStatement getCreateStatement(Caregiver caregiver) {
        PreparedStatement preparedStatement = null;
        try {
            final String SQL = "INSERT INTO caregiver (firstname, surname, telephoneNumber, isLocked) " +
                    "VALUES (?, ?, ?, ?)";
            preparedStatement = this.connection.prepareStatement(SQL);
            preparedStatement.setString(1, caregiver.getFirstName());
            preparedStatement.setString(2, caregiver.getSurname());
            preparedStatement.setString(3, caregiver.getTelephoneNumber());
            preparedStatement.setInt(4, caregiver.getIsLocked());
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return preparedStatement;
    }

    /**
     * Generates a PreparedStatement to query a caregiver by a given caregiver id (cid).
     *
     * @param cid Caregiver id to query.
     * @return PreparedStatement to query the caregiver.
     */
    @Override
    protected PreparedStatement getReadByIDStatement(long cid) {
        PreparedStatement preparedStatement = null;
        try {
            final String SQL = "SELECT * FROM caregiver WHERE cid = ?";
            preparedStatement = this.connection.prepareStatement(SQL);
            preparedStatement.setLong(1, cid);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return preparedStatement;
    }

    /**
     * Maps a ResultSet of one caregiver to an object of Caregiver.
     *
     * @param result ResultSet with a single row. Columns will be mapped to an object of class Caregiver.
     * @return Object of class Caregiver with the data from the resultSet.
     */
    @Override
    protected Caregiver getInstanceFromResultSet(ResultSet result) throws SQLException {
        return new Caregiver(result.getInt(1),
                             result.getString(2),
                             result.getString(3),
                             result.getString(4),
                             result.getInt(5));
    }

    /**
     * Generates a PreparedStatement to query all caregivers.
     *
     * @return PreparedStatement to query all caregivers.
     */
    @Override
    protected PreparedStatement getReadAllStatement() {
        PreparedStatement statement = null;
        try {
            final String SQL = "SELECT * FROM caregiver WHERE isLocked = 0";
            statement = this.connection.prepareStatement(SQL);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return statement;
    }

    /**
     * Maps a ResultSet of all caregivers to an ArrayList of Caregiver objects.
     *
     * @param result ResultSet with all rows. The Columns will be mapped to objects of class Caregiver.
     * @return ArrayList with objects of class Caregiver of all rows in the ResultSet.
     */
    @Override
    protected ArrayList<Caregiver> getListFromResultSet(ResultSet result) throws SQLException {
        ArrayList<Caregiver> list = new ArrayList<>();
        while (result.next()) {
            Caregiver caregiver = new Caregiver(result.getInt(1), result.getString(2),
                    result.getString(3), result.getString(4), result.getInt(5));
            list.add(caregiver);
        }
        return list;
    }

    /**
     * Generates a PreparedStatement to update the given caregiver, identified
     * by the id of the caregiver (cid).
     *
     * @param caregiver Caregiver object to update.
     * @return PreparedStatement to update the given caregiver.
     */
    @Override
    protected PreparedStatement getUpdateStatement(Caregiver caregiver) {
        PreparedStatement preparedStatement = null;
        try {
            final String SQL =
                "UPDATE caregiver SET " +
                       "firstname = ?, " +
                       "surname = ?, " +
                       "telephoneNumber = ?, " +
                       "isLocked = ? " +
                       "WHERE cid = ?";
            preparedStatement = this.connection.prepareStatement(SQL);
            preparedStatement.setString(1, caregiver.getFirstName());
            preparedStatement.setString(2, caregiver.getSurname());
            preparedStatement.setString(3, caregiver.getTelephoneNumber());
            preparedStatement.setLong(4, caregiver.getIsLocked());
            preparedStatement.setLong(5, caregiver.getCid());
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return preparedStatement;
    }

    /**
     * Generates a PreparedStatement to delete a caregiver with the given id.
     *
     * @param cid ID of the caregiver to delete.
     * @return PreparedStatement to delete caregiver with the given id.
     */
    @Override
    protected PreparedStatement getDeleteStatement(long cid) {
        PreparedStatement preparedStatement = null;
        try {
            final String SQL = "DELETE FROM caregiver WHERE cid = ?";
            preparedStatement = this.connection.prepareStatement(SQL);
            preparedStatement.setLong(1, cid);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return preparedStatement;
    }
    /**
     * Method to block a caregiver, as long as the timespan is less than 10 years of the last treatment.
     * Expires the timespan of 10 years, the caregiver gets deleted.
     * @param cid Caregiver ID to be blocked
     */
    public void blockCaregiver(long cid) {
        String sqlBlockCaregiver = "UPDATE caregiver SET isLocked = 1 WHERE cid = ?";
        try(PreparedStatement preparedStatement = this.connection.prepareStatement(sqlBlockCaregiver)){
            preparedStatement.setLong(1, cid);
            preparedStatement.executeUpdate();
        }catch (Exception exception){
            exception.printStackTrace();
        }
    }
}
