package de.hitec.nhplus.datastorage;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The <code>DaoImp</code> provides the logic for implementing the logic from the <code>Dao</code>
 * @param <T> Template Object for the different Instances of the Models
 */

public abstract class DaoImp<T> implements Dao<T> {

    /**
     * Provides the activated connection constructor
     */

    protected Connection connection;

    /**
     * Constructor for the Connection
     * @param connection Object of Connection to execute the SQL-statements.
     */

    public DaoImp(Connection connection) {
        this.connection = connection;
    }

    /**
     * Used to create the different Models
     * @param t Holds some Values for the Database
     * @throws SQLException Throws an SQL exception if an SQL-Error occurs.
     */

    @Override
    public void create(T t) throws SQLException {
        getCreateStatement(t).executeUpdate();
    }

    /**
     * Reads information of the Database
     * @param key holds information for the Database
     * @return required object
     * @throws SQLException Throws an SQL exception if an SQL-Error occurs.
     */

    @Override
    public T read(long key) throws SQLException {
        T object = null;
        ResultSet result = getReadByIDStatement(key).executeQuery();
        if (result.next()) {
            object = getInstanceFromResultSet(result);
        }
        return object;
    }

    /**
     * Reads information of the Database
     * @return List of required object
     * @throws SQLException Throws an SQL exception if an SQL-Error occurs.
     */

    @Override
    public List<T> readAll() throws SQLException {
        return getListFromResultSet(getReadAllStatement().executeQuery());
    }

    /**
     * Updates information of the Database
     * @param t hold the new information for the database
     * @throws SQLException Throws an SQL exception if an SQL-Error occurs.
     */

    @Override
    public void update(T t) throws SQLException {
        getUpdateStatement(t).executeUpdate();
    }

    /**
     * Delete Information from Database
     * @param key holds the ID of the target
     * @throws SQLException Throws an SQL exception if an SQL-Error occurs.
     */

    @Override
    public void deleteById(long key) throws SQLException {
        getDeleteStatement(key).executeUpdate();
    }

    /**
     * Provide Method to get required Instance as ResultSet
     * @param set Object of ResultSet to get Data from
     * @return Instance
     * @throws SQLException Throws an SQL exception if an SQL-Error occurs.
     */
    protected abstract T getInstanceFromResultSet(ResultSet set) throws SQLException;

    /**
     * Provide Method to return a ArrayList of required Instance
     * @param set Object of ResultSet to get Data from
     * @return ArrayList
     * @throws SQLException Throws an SQL exception if an SQL-Error occurs.
     */

    protected abstract ArrayList<T> getListFromResultSet(ResultSet set) throws SQLException;

    /**
     * Provide Method to create a PreparedStatement of an Instance in the Database
     * @param t Template Object for the different Instances of the Models
     * @return preparedStatement
     */

    protected abstract PreparedStatement getCreateStatement(T t);

    /**
     * Provide Method to read from a database by the id
     * @param key parameter for the id of the instance
     * @return PreparedStatement
     */

    protected abstract PreparedStatement getReadByIDStatement(long key);

    /**
     * Provide Method to read everything from a database
     * @return PreparedStatement
     */

    protected abstract PreparedStatement getReadAllStatement();

    /**
     * Provide Method to update an Instance in the database
     * @param t Template Object for the different Instances of the Models
     * @return PreparedStatement
     */

    protected abstract PreparedStatement getUpdateStatement(T t);

    /**
     * Provide Method to delete an Instance from the database by the id
     * @param key parameter for the id of the instance
     * @return PreparedStatement
     */

    protected abstract PreparedStatement getDeleteStatement(long key);
}
