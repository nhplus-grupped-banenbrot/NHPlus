package de.hitec.nhplus.datastorage;

import de.hitec.nhplus.model.Patient;
import de.hitec.nhplus.utils.DateConverter;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Implements the Interface DaoImp. Overrides methods to generate specific PreparedStatements,
 * to execute the specific SQL Statements.
 */
public class PatientDao extends DaoImp<Patient> {

    /**
     * The constructor initiates an object of PatientDao and passes the connection to its super class.
     *
     * @param connection Object of Connection to execute the SQL-statements.
     */
    public PatientDao(Connection connection) {
        super(connection);
    }

    /**
     * Generates a PreparedStatement to persist the given object of Patient.
     *
     * @param patient Object of Patient to persist.
     * @return PreparedStatement to insert the given patient.
     */
    @Override
    protected PreparedStatement getCreateStatement(Patient patient) {
        PreparedStatement preparedStatement = null;
        try {
            final String SQL = "INSERT INTO patient (firstname, surname, dateOfBirth, carelevel, roomnumber, createdOn, isLocked) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?)";
            preparedStatement = this.connection.prepareStatement(SQL);
            preparedStatement.setString(1, patient.getFirstName());
            preparedStatement.setString(2, patient.getSurname());
            preparedStatement.setString(3, patient.getDateOfBirth());
            preparedStatement.setString(4, patient.getCareLevel());
            preparedStatement.setString(5, patient.getRoomNumber());
            preparedStatement.setString(6, patient.getCreatedOn());
            preparedStatement.setInt(7, patient.isLocked());
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return preparedStatement;
    }

    /**
     * Generates a PreparedStatement to query a patient by a given patient id (pid).
     *
     * @param pid Patient id to query.
     * @return PreparedStatement to query the patient.
     */
    @Override
    protected PreparedStatement getReadByIDStatement(long pid) {
        PreparedStatement preparedStatement = null;
        try {
            final String SQL = "SELECT * FROM patient WHERE pid = ?";
            preparedStatement = this.connection.prepareStatement(SQL);
            preparedStatement.setLong(1, pid);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return preparedStatement;
    }

    /**
     * Maps a ResultSet of one patient to an object of Patient.
     *
     * @param result ResultSet with a single row. Columns will be mapped to an object of class Patient.
     * @return Object of class Patient with the data from the resultSet.
     */
    @Override
    protected Patient getInstanceFromResultSet(ResultSet result) throws SQLException {
        return new Patient(
                result.getInt(1),
                result.getString(2),
                result.getString(3),
                DateConverter.convertStringToLocalDate(result.getString(4)),
                result.getString(5),
                result.getString(6),
                DateConverter.convertStringToLocalDate(result.getString(7)),
                result.getInt(8));
    }

    /**
     * Generates a PreparedStatement to query all patients.
     *
     * @return PreparedStatement to query all patients.
     */
    @Override
    protected PreparedStatement getReadAllStatement() {
        PreparedStatement statement = null;
        try {
            final String SQL = "SELECT * FROM patient WHERE isLocked = 0";
            statement = this.connection.prepareStatement(SQL);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return statement;
    }

    /**
     * Generates a PreparedStatement to query all patient data for a patient.
     *
     * @param patient The patient to query all patient data from.
     * @return PreparedStatement to query all patient data for a patient.
     */
    protected PreparedStatement getReadPatientDataStatement(Patient patient) {
        PreparedStatement preparedStatement = null;
        try {
            final String SQL = "SELECT p.firstname, p.surname, p.dateOfBirth, " +
                                      "p.carelevel, p.roomnumber, p.createdOn, " +
                                      "c.firstname, c.surname, " +
                                      "t.treatment_date, t.begin, t.end, " +
                                      "t.description, t.remark " +
                                      "from patient as p " +
                               "LEFT JOIN treatment as t ON p.pid = t.pid " +
                               "LEFT JOIN caregiver as c ON t.cid = c.cid " +
                               "WHERE p.pid = ?";
            preparedStatement = this.connection.prepareStatement(SQL);
            preparedStatement.setLong(1, patient.getPid());
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return preparedStatement;
    }

    /**
     * Queries all patient data (the patient itself and all their treatments)
     *
     * @param patient The patient to query all patient data from.
     * @throws SQLException Throws an SQL exception if an SQL-Error occurs.
     * @return ResultSet containing the query result form SQL
     */
    public ResultSet readPatientData(Patient patient) throws SQLException {
        return getReadPatientDataStatement(patient).executeQuery();
    }

    /**
     * Maps a ResultSet of all patients to an ArrayList of Patient objects.
     *
     * @param result ResultSet with all rows. The Columns will be mapped to objects of class Patient.
     * @return ArrayList with objects of class Patient of all rows in the ResultSet.
     */
    @Override
    protected ArrayList<Patient> getListFromResultSet(ResultSet result) throws SQLException {
        ArrayList<Patient> list = new ArrayList<>();
        while (result.next()) {
            LocalDate date = DateConverter.convertStringToLocalDate(result.getString(4));
            Patient patient = new Patient(
                    result.getInt(1),
                    result.getString(2),
                    result.getString(3),
                    date,
                    result.getString(5),
                    result.getString(6),
                    DateConverter.convertStringToLocalDate(result.getString(7)),
                    result.getInt(8));
            list.add(patient);
        }
        return list;
    }

    /**
     * Generates a PreparedStatement to update the given patient, identified
     * by the id of the patient (pid).
     *
     * @param patient Patient object to update.
     * @return PreparedStatement to update the given patient.
     */
    @Override
    protected PreparedStatement getUpdateStatement(Patient patient) {
        PreparedStatement preparedStatement = null;
        try {
            final String SQL =
                    "UPDATE patient SET " +
                            "firstname = ?, " +
                            "surname = ?, " +
                            "dateOfBirth = ?, " +
                            "carelevel = ?, " +
                            "roomnumber = ? " +
                            "WHERE pid = ?";
            preparedStatement = this.connection.prepareStatement(SQL);
            preparedStatement.setString(1, patient.getFirstName());
            preparedStatement.setString(2, patient.getSurname());
            preparedStatement.setString(3, patient.getDateOfBirth());
            preparedStatement.setString(4, patient.getCareLevel());
            preparedStatement.setString(5, patient.getRoomNumber());
            preparedStatement.setLong(6, patient.getPid());
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return preparedStatement;
    }

    /**
     * Generates a PreparedStatement to delete a patient with the given id.
     *
     * @param pid ID of the patient to delete.
     * @return PreparedStatement to delete patient with the given id.
     */
    @Override
    protected PreparedStatement getDeleteStatement(long pid) {
        PreparedStatement preparedStatement = null;
        try {
            final String SQL = "DELETE FROM patient WHERE pid = ?";
            preparedStatement = this.connection.prepareStatement(SQL);
            preparedStatement.setLong(1, pid);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return preparedStatement;
    }


    /**
     * Method to block a patient, as long as the timespan is less than 10 years of the last treatment.
     * Expires the timespan of 10 years, the patient gets deleted.
     * @param pid Patient ID to be blocked
     */
    public void blockPatient(long pid) {
        String sqlBlockPatient = "UPDATE patient SET isLocked = 1 WHERE pid = ?";
        try(PreparedStatement preparedStatement = this.connection.prepareStatement(sqlBlockPatient)){
            preparedStatement.setLong(1, pid);
            preparedStatement.executeUpdate();
        }catch (Exception exception){
            exception.printStackTrace();
        }
    }
}
