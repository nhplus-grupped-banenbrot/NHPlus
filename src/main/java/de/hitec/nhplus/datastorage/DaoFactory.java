package de.hitec.nhplus.datastorage;

/**
 * Builds connections to a specific data bank
 */
public class DaoFactory {
    /**
     * Instance of DaoFactory
     */
    private static DaoFactory instance;

    /**
     * Private Constructor
     */
    private DaoFactory() {
    }

    /**
     * Checks if an instance of DaoFactory exists. If there is no instance, it creates a new one.
     * @return Gives back (new) instance
     */
    public static DaoFactory getDaoFactory() {
        if (DaoFactory.instance == null) {
            DaoFactory.instance = new DaoFactory();
        }
        return DaoFactory.instance;
    }

    /**
     * Creating a connection to treatment data bank
     * @return Creates a new connection to the treatment data bank
     */
    public TreatmentDao createTreatmentDao() {
        return new TreatmentDao(ConnectionBuilder.getConnection());
    }
    /**
     * Creating a connection to patient data bank
     * @return Creates a new connection to the patient data bank
     */
    public PatientDao createPatientDAO() {
        return new PatientDao(ConnectionBuilder.getConnection());
    }
    /**
     * Creating a connection to caregiver data bank
     * @return Creates a new connection to the caregiver data bank
     */
    public CaregiverDao createCaregiverDAO() {
        return new CaregiverDao(ConnectionBuilder.getConnection());
    }
    /**
     * Creating a connection to user data bank
     * @return Creates a new connection to the user data bank
     */
    public UserDao createUserDAO() {
        return new UserDao(ConnectionBuilder.getConnection());
    }
}
