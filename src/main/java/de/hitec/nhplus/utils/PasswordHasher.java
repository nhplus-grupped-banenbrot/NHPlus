package de.hitec.nhplus.utils;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 * Helper class to create password hashes from plaintext passwords.
 * This also uses a password salt to further enhance security.
 */
public class PasswordHasher {
    private static final String SALT = "asdf$%&(/56fdgskrelk[}";

    /**
     * Default constructor for passwordHasher
     */
    public PasswordHasher(){}

    /**
     * Create a password hash from a plaintext password.
     * This uses the PBKDF2WithHmacSHA512 algorithm with a hardcoded password salt.
     * The hashed password is Base64 encoded to allow it to be easily stored in a database.
     * 
     * @param password The plaintext password to create the password hash from
     * @throws NoSuchAlgorithmException Throws NoSuchAlgorithmException if the algorithm could not be found.
     * @throws InvalidKeySpecException Throws InvalidKeySpecException if the key specification is inappropriate
     * @return The hashed password
     */
    public static String getHashedPassword(String password) throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        byte salt[] = SALT.getBytes(StandardCharsets.UTF_8);
        int iterations = 1000;

        PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterations, 512);
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
        byte hash[] = factory.generateSecret(spec).getEncoded();

        return Base64.getEncoder().encodeToString(hash);
    }
}
