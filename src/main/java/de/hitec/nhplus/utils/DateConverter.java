package de.hitec.nhplus.utils;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Locale;

/**
 * Helper class to convert between Strings and LocalDate/LocalTime
 */
public class DateConverter {

    private static final String DATE_FORMAT = "dd.MM.yyyy";
    private static final String TIME_FORMAT = "HH:mm";
    private static final DateTimeFormatter FORMATTER = new DateTimeFormatterBuilder().appendPattern(DATE_FORMAT).toFormatter(Locale.GERMANY);

    /**
     * Default constructor for DateConverter
     */
    public DateConverter() {}

    /**
     * Convert a String to LocalDate
     * 
     * @param date The date in form of a string to convert from
     * @return The converted date as type LocalDate
     */
    public static LocalDate convertStringToLocalDate(String date) {
        return LocalDate.parse(date, FORMATTER);
    }

    /**
     * Convert a String to LocalTime
     * 
     * @param time The time in form of a string to convert from
     * @return The converted time as type LocalTime
     */
    public static LocalTime convertStringToLocalTime(String time) {
        return LocalTime.parse(time, DateTimeFormatter.ofPattern(TIME_FORMAT, Locale.GERMANY));
    }

    /**
     * Convert a LocalDate to a String
     * 
     * @param date The date in form of a LocalDate to convert from
     * @return The converted date as String
     */
    public static String convertLocalDateToString(LocalDate date) {
        return date.format(DateTimeFormatter.ofPattern(DATE_FORMAT, Locale.GERMANY));
    }

    /**
     * Convert a LocalTime to a String
     * 
     * @param time The time in form of a LocalTime to convert from
     * @return The converted time as String
     */
    public static String convertLocalTimeToString(LocalTime time) {
        return time.format(DateTimeFormatter.ofPattern(TIME_FORMAT, Locale.GERMANY));
    }
}
