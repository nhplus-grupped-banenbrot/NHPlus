package de.hitec.nhplus.utils;

import de.hitec.nhplus.datastorage.ConnectionBuilder;
import de.hitec.nhplus.datastorage.DaoFactory;
import de.hitec.nhplus.datastorage.CaregiverDao;
import de.hitec.nhplus.datastorage.PatientDao;
import de.hitec.nhplus.datastorage.TreatmentDao;
import de.hitec.nhplus.datastorage.UserDao;
import de.hitec.nhplus.model.Caregiver;
import de.hitec.nhplus.model.Patient;
import de.hitec.nhplus.model.Treatment;
import de.hitec.nhplus.model.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

import static de.hitec.nhplus.utils.DateConverter.convertStringToLocalDate;
import static de.hitec.nhplus.utils.DateConverter.convertStringToLocalTime;

/**
 * Call static class provides to static methods to set up and wipe the database. It uses the class ConnectionBuilder
 * and its path to build up the connection to the database. The class is executable. Executing the class will build
 * up a connection to the database and calls setUpDb() to wipe the database, build up a clean database and fill the
 * database with some test data.
 */
public class SetUpDB {

    /**
     * Default constructor for SetUpDB
     */
    public SetUpDB(){}

    /**
     * This method wipes the database by dropping the tables. Then the method calls DDL statements to build it up from
     * scratch and DML statements to fill the database with hard coded test data.
     */
    public static void setUpDb() {
        Connection connection = ConnectionBuilder.getConnection();
        SetUpDB.wipeDb(connection);
        SetUpDB.setUpTablePatient(connection);
        SetUpDB.setUpTableCaregiver(connection);
        SetUpDB.setUpTableTreatment(connection);
        SetUpDB.setUpTableUser(connection);
        SetUpDB.setUpPatients();
        SetUpDB.setUpCaregivers();
        SetUpDB.setUpTreatments();
        SetUpDB.setUpUsers();
    }

    /**
     * This method wipes the database by dropping the tables.
     * 
     * @param connection holds the connection to the database nursingHome
     */
    public static void wipeDb(Connection connection) {
        try (Statement statement = connection.createStatement()) {
            statement.execute("DROP TABLE patient");
            statement.execute("DROP TABLE treatment");
            statement.execute("DROP TABLE caregiver");
            statement.execute("DROP TABLE user");
        } catch (SQLException exception) {
            System.out.println(exception.getMessage());
        }
    }

    /**
     * When this method is called create in nursingHome a new table called patient
     * with the columns pid, firstname, surname, dateOfbirth, Carelevel, roomnumber, createdOn and isLocked.
     *
     * @param connection holds the connection to the database nursingHome
     */
    private static void setUpTablePatient(Connection connection) {
        final String SQL = "CREATE TABLE IF NOT EXISTS patient (" +
                "   pid INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "   firstname TEXT NOT NULL, " +
                "   surname TEXT NOT NULL, " +
                "   dateOfBirth TIMESTAMP NOT NULL, " +
                "   carelevel TEXT NOT NULL, " +
                "   roomnumber TEXT NOT NULL, " +
                "   createdOn TIMESTAMP NOT NULL," +
                "   isLocked INTEGER NOT NULL" +
                ");";
        try (Statement statement = connection.createStatement()) {
            statement.execute(SQL);
        } catch (SQLException exception) {
            System.out.println(exception.getMessage());
        }
    }

    /**
     * When this method is called create in nursingHome a new table called Caregiver
     * with the columns cid, firstname, surname, telephoneNumber and isLocked.
     *
     * @param connection holds the connection to the database nursingHome
     */
    private static void setUpTableCaregiver(Connection connection) {
        final String SQL = "CREATE TABLE IF NOT EXISTS caregiver (" +
                "   cid INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "   firstname TEXT NOT NULL, " +
                "   surname TEXT NOT NULL, " +
                "   telephoneNumber TEXT NOT NULL, " +
                "   isLocked INTEGER NOT NULL" +
                ");";
        try (Statement statement = connection.createStatement()) {
            statement.execute(SQL);
        } catch (SQLException exception) {
            System.out.println(exception.getMessage());
        }
    }

    /**
     * When this method is called create in nursingHome a new table called treatment
     * with the columns tid, pid with a REFERENCES to the Table patient, cid with a REFERENCES to the Table
     * Caregiver, treatment_date, begin, end, description and remark.
     *
     * @param connection holds the connection to the database nursingHome
     */
    private static void setUpTableTreatment(Connection connection) {
        final String SQL = "CREATE TABLE IF NOT EXISTS treatment (" +
                "   tid INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "   pid INTEGER NOT NULL, " +
                "   cid INTEGER not null ," +
                "   treatment_date TIMESTAMP NOT NULL, " +
                "   begin TEXT NOT NULL, " +
                "   end TEXT NOT NULL, " +
                "   description TEXT NOT NULL, " +
                "   remark TEXT NOT NULL," +
                "   FOREIGN KEY (pid) REFERENCES patient (pid) ON DELETE CASCADE ," +
                "   FOREIGN KEY  (cid) REFERENCES caregiver (cid) ON DELETE CASCADE "+
                ");";

        try (Statement statement = connection.createStatement()) {
            statement.execute(SQL);
        } catch (SQLException exception) {
            System.out.println(exception.getMessage());
        }
    }


    /**
     * When this method is called create in nursingHome a new table called User
     * with the columns uid, username, passwordHash and accessLevel.
     *
     * @param connection holds the connection to the database nursingHome
     */
    private static void setUpTableUser(Connection connection) {
        final String SQL = "CREATE TABLE IF NOT EXISTS user (" +
                "   uid INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "   username TEXT NOT NULL UNIQUE, " +
                "   passwordHash TEXT NOT NULL, " +
                "   accessLevel INTEGER NOT NULL" +
                ");";
        try (Statement statement = connection.createStatement()) {
            statement.execute(SQL);
        } catch (SQLException exception) {
            System.out.println(exception.getMessage());
        }
    }

    /**
     * When this method is called it will creat Patient data for the patient in nursingHome
     */
    private static void setUpPatients() {
        try {
            PatientDao dao = DaoFactory.getDaoFactory().createPatientDAO();
            dao.create(new Patient("Seppl", "Herberger", convertStringToLocalDate("14.07.1958"), "4", "202", LocalDate.of(2024,5,27),0));
            dao.create(new Patient("Martina", "Gerdsen", convertStringToLocalDate("13.05.1985"), "5", "010",LocalDate.of(2024,5,27),0));
            dao.create(new Patient("Gertrud", "Franzen", convertStringToLocalDate("01.05.1991"), "3", "002", LocalDate.of(2024,5,27),0));
            dao.create(new Patient("Ahmet", "Yilmaz", convertStringToLocalDate("15.04.1976"), "3", "013", LocalDate.of(2024,5,27),0));
            dao.create(new Patient("Hans", "Neumann", convertStringToLocalDate("01.07.1950"), "2", "001", LocalDate.of(2024,5,27),0));
            dao.create(new Patient("Elisabeth", "Müller", convertStringToLocalDate("27.03.1964"), "5", "110", LocalDate.of(2024,5,27),0));
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * When this method is called it will creat Caregiver data for the patient in nursinghome
     */
    private static void setUpCaregivers() {
        try {
            CaregiverDao dao = DaoFactory.getDaoFactory().createCaregiverDAO();
            dao.create(new Caregiver("Heinz", "Peter", "01642214784", 0));
            dao.create(new Caregiver("Berta","Grunde","01642214785",0));
            dao.create(new Caregiver("Nick", "Sänger","01642214786",0));
            dao.create(new Caregiver("Mika", "Matschulla","01642214787",0));
            dao.create(new Caregiver("Jan-Malte", "von Soosten","01642214788",0));
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }


    /**
     * When this method is called it will creat Treatment data in treatments in nursingHome
     */
    private static void setUpTreatments() {
        try {
            TreatmentDao dao = DaoFactory.getDaoFactory().createTreatmentDao();
            dao.create(new Treatment(1, 1,2, convertStringToLocalDate("01.06.2023"), convertStringToLocalTime("11:00"), convertStringToLocalTime("15:00"), "Gespräch", "Der Patient hat enorme Angstgefühle und glaubt, er sei überfallen worden. Ihm seien alle Wertsachen gestohlen worden.\nPatient beruhigt sich erst, als alle Wertsachen im Zimmer gefunden worden sind."));
            dao.create(new Treatment(2, 1,1, convertStringToLocalDate("02.07.2023"), convertStringToLocalTime("11:00"), convertStringToLocalTime("12:30"), "Gespräch", "Patient irrt auf der Suche nach gestohlenen Wertsachen durch die Etage und bezichtigt andere Bewohner des Diebstahls.\nPatient wird in seinen Raum zurückbegleitet und erhält Beruhigungsmittel."));
            dao.create(new Treatment(3, 2,1, convertStringToLocalDate("03.08.2023"), convertStringToLocalTime("07:30"), convertStringToLocalTime("08:00"), "Waschen", "Patient mit Waschlappen gewaschen und frisch angezogen. Patient gewendet."));
            dao.create(new Treatment(4, 1,1, convertStringToLocalDate("04.09.2023"), convertStringToLocalTime("15:10"), convertStringToLocalTime("16:00"), "Spaziergang", "Spaziergang im Park, Patient döst  im Rollstuhl ein"));
            dao.create(new Treatment(8, 1,1, convertStringToLocalDate("05.10.2023"), convertStringToLocalTime("15:00"), convertStringToLocalTime("16:00"), "Spaziergang", "Parkspaziergang; Patient ist heute lebhafter und hat klare Momente; erzählt von seiner Tochter"));
            dao.create(new Treatment(9, 2,1, convertStringToLocalDate("06.11.2023"), convertStringToLocalTime("11:00"), convertStringToLocalTime("11:30"), "Waschen", "Waschen per Dusche auf einem Stuhl; Patientin gewendet;"));
            dao.create(new Treatment(12, 5,1, convertStringToLocalDate("06.08.2023"), convertStringToLocalTime("15:00"), convertStringToLocalTime("15:30"), "Physiotherapie", "Übungen zur Stabilisation und Mobilisierung der Rückenmuskulatur"));
            dao.create(new Treatment(14, 4,1, convertStringToLocalDate("07.09.2023"), convertStringToLocalTime("09:30"), convertStringToLocalTime("10:15"), "KG", "Lympfdrainage"));
            dao.create(new Treatment(16, 6,1, convertStringToLocalDate("08.09.2023"), convertStringToLocalTime("13:30"), convertStringToLocalTime("13:45"), "Toilettengang", "Hilfe beim Toilettengang; Patientin klagt über Schmerzen beim Stuhlgang. Gabe von Iberogast"));
            dao.create(new Treatment(17, 6,1, convertStringToLocalDate("09.10.2023"), convertStringToLocalTime("16:00"), convertStringToLocalTime("17:00"), "KG", "Massage der Extremitäten zur Verbesserung der Durchblutung"));
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * when this method is called it will creat User data in user in nursingHome
     */
    private static void setUpUsers() {
        try {
            UserDao dao = DaoFactory.getDaoFactory().createUserDAO();
            dao.create(new User("Heimleitung", PasswordHasher.getHashedPassword("123456"), 2));
            dao.create(new User("PflegerHeinz", PasswordHasher.getHashedPassword("12"), 1));
            dao.create(new User("PflegerBerta", PasswordHasher.getHashedPassword("asdf"), 1));
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Recreates all tables and example entries in the database nursingHome
     * @param args The provided command line arguments for the program.
     */
    public static void main(String[] args) {
        SetUpDB.setUpDb();
    }
}
